// https://babeljs.io/docs/en/configuration#javascript-configuration-files

const presets = [
  "@babel/preset-env",
  "@babel/preset-react"
]

module.exports = { presets };
