const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  // Use multiple named entry points, one per component.
  entry: {
    Another: './src/Another.jsx',
    Hello: './src/Hello.jsx',
  },
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: '[name].js',
    // Indicates that the output is to be a module
    libraryTarget: 'commonjs2',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' },
      },
    ],
  },
  plugins: [
    new UglifyJsPlugin(),
  ],
  // Indicates which packages to exclude from the bundle
  externals: [
    'react',
  ],
};