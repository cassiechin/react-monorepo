import React from 'react';
import ReactDOM from 'react-dom';
import { Catalog, pageLoader } from 'catalog';
import Another from '@react-monorepo/components/lib/Another';
import Hello from '@react-monorepo/components/lib/Hello';

ReactDOM.render(
  <Catalog
    title='Catalog'
    pages={[
      {
        imports: { Another, Hello },
        path: '/',
        title: 'Introduction',
        content: pageLoader('intro.md'),
      },
    ]}
  />,
  document.getElementById('root')
);
