# README.md

## Setup

- `git init`
- `yarn init`
    - Set `"private": true`
    - Set `"workspaces": ["packages/*"]`
- `yarn add -D lerna; ./node_modules/.bin/lerna init`
    - Set `"npmClient": "yarn"`
    - Set `"useWorkspaces": true`

### @react-monorepo/components (packages/components)

- The `postinstall` command is triggered when we use the `lerna bootstrap`
  command (when another person is downloading the project).

### @react-monorepo/project (packages/project)

- `yarn workspace @react-monorepo/project add @react-monorepo/components@1.0.0`

### @react-monorepo/styleguide (packages/styleguide)

- `yarn workspace @react-monorepo/styleguide add @react-monorepo/components@1.0.0`

## Issues I ran into

- Yarn workspaces not able to add local packages
    - You need to include the @version when adding a local package
    - <https://github.com/yarnpkg/yarn/issues/4878>

## Resources

- <https://codeburst.io/monorepos-by-example-part-3-1ebdea7ccbea>